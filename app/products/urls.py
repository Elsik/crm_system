from django.urls import path
from .views import ProductList, ProductDelete, ProductCreate, ProductDetail, ProductUpdate

urlpatterns = [
    path('', ProductList.as_view(), name='products-list'),
    path('<int:pk>/delete/', ProductDelete.as_view(), name='products-delete'),
    path('new/', ProductCreate.as_view(), name='products-create'),
    path('<int:pk>/', ProductDetail.as_view(), name='products-detail.html'),
    path('<int:pk>/edit/', ProductUpdate.as_view(), name='products-edit.html'),
]
