from django.shortcuts import render
from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView
from django.urls import reverse_lazy

from .models import Product


class ProductList(ListView):
    model = Product
    template_name = 'products/products-list.html'
    context_object_name = 'products'


class ProductDelete(DeleteView):
    model = Product
    template_name = 'products/products-delete.html'
    success_url = reverse_lazy('products-list')


class ProductCreate(CreateView):
    model = Product
    template_name = 'products/products-create.html'
    fields = ['name', 'description', 'cost']
    success_url = reverse_lazy('products-list')


class ProductDetail(DetailView):
    model = Product
    template_name = 'products/products-detail.html'


class ProductUpdate(UpdateView):
    model = Product
    template_name = 'products/products-edit.html'
    fields = ['name', 'description', 'cost']
    success_url = reverse_lazy('products-list')
