from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    description = models.CharField(max_length=100, verbose_name='Описание')
    cost = models.CharField(max_length=100, default=0, verbose_name='Цена')

    def __str__(self):
        return self.name