# Generated by Django 5.0.2 on 2024-02-19 13:22

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contract',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Название')),
                ('document_file', models.FileField(null=True, upload_to='contract/documents/', verbose_name='Документ файл')),
                ('start_date', models.DateTimeField(verbose_name='Начальная дата')),
                ('end_date', models.DateTimeField(verbose_name='Конечная дата')),
                ('cost', models.CharField(default=0, max_length=100)),
                ('products', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='products.product', verbose_name='Продукты')),
            ],
        ),
    ]
