from django.db import models

from products.models import Product


class Contract(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    products = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Продукты')
    document_file = models.FileField(null=True, upload_to='contract/documents/', verbose_name='Документ файл')
    start_date = models.DateTimeField(verbose_name='Начальная дата')
    end_date = models.DateTimeField(verbose_name='Конечная дата')
    cost = models.IntegerField(default=0, verbose_name='Цена')

    def __str__(self):
        return f'Контракт№{self.id}: {self.name}'
