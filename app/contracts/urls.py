from django.urls import path
from .views import ContractList, ContractCreate, ContractDelete, ContractDetail, ContractUpdate

urlpatterns = [
    path('', ContractList.as_view(), name='contracts-list'),
    path('new/', ContractCreate.as_view(), name='contracts-create'),
    path('<int:pk>/delete/', ContractDelete.as_view(), name='contracts-delete'),
    path('<int:pk>/', ContractDetail.as_view(), name='contracts-detail.html'),
    path('<int:pk>/edit/', ContractUpdate.as_view(), name='contracts-edit.html')
]