from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, DeleteView, DetailView, UpdateView
from .models import Contract


class ContractList(ListView):
    model = Contract
    template_name = 'contracts/contracts-list.html'
    context_object_name = 'contracts'


class ContractCreate(CreateView):
    model = Contract
    template_name = 'contracts/contracts-create.html'
    fields = ['name', 'products', 'document_file', 'start_date', 'end_date', 'cost']
    success_url = reverse_lazy('contracts-list')


class ContractDelete(DeleteView):
    model = Contract
    template_name = 'contracts/contracts-delete.html'
    success_url = reverse_lazy('contracts-list')


class ContractDetail(DetailView):
    model = Contract
    template_name = 'contracts/contracts-detail.html'


class ContractUpdate(UpdateView):
    model = Contract
    template_name = 'contracts/contracts-edit.html'
    fields = ['name', 'products', 'document_file', 'start_date', 'end_date', 'cost']
    success_url = reverse_lazy('contracts-list')

