from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, DeleteView, DetailView, UpdateView

from .models import Lead


class LeadCreate(CreateView):
    model = Lead
    template_name = 'leads/leads-create.html'
    fields = ['first_name', 'last_name', 'phone', 'email', 'ads_company']
    success_url = reverse_lazy('leads-list')


class LeadDelete(DeleteView):
    model = Lead
    template_name = 'leads/leads-delete.html'
    success_url = reverse_lazy('leads-list')


class LeadDetail(DetailView):
    model = Lead
    template_name = 'leads/leads-detail.html'


class LeadUpdate(UpdateView):
    model = Lead
    template_name = 'leads/leads-edit.html'
    fields = ['first_name', 'last_name', 'phone', 'email', 'ads_company']
    success_url = reverse_lazy('leads-list')


class LeadList(ListView):
    model = Lead
    template_name = 'leads/leads-list.html'
    context_object_name = 'leads'
