from django.urls import path
from .views import LeadList, LeadDelete, LeadDetail, LeadUpdate, LeadCreate

urlpatterns = [
    path('', LeadList.as_view(), name='leads-list'),
    path('new/', LeadCreate.as_view(), name='leads-create'),
    path('<int:pk>/delete/', LeadDelete.as_view(), name='leads-delete'),
    path('<int:pk>', LeadDetail.as_view(), name='leads-detail'),
    path('<int:pk>/edit/', LeadUpdate.as_view(), name='leads-edit')
]