from django.db import models

from advertisements.models import Advertisements


class Lead(models.Model):
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    last_name = models.CharField(max_length=100, verbose_name='Фамилия')
    phone = models.CharField(max_length=12, verbose_name='Номер телефона')
    email = models.CharField(max_length=100)
    ads_company = models.ForeignKey(Advertisements, on_delete=models.CASCADE, verbose_name='Рекламная кампания, из которой он '
                                                                               'узнал об услуге.')

    def __str__(self):
        return f'Lead№{self.id}: {self.last_name} {self.first_name}'
