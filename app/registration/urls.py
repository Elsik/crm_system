from django.contrib.auth.views import LoginView
from django.urls import path

from .views import logout_view

urlpatterns = [
    path('logout/', logout_view, name='logout'),
    path('login/', LoginView.as_view(template_name='registration/login.html'), name='login'),
]