from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, DeleteView, DetailView, UpdateView

from .models import Customer


class CustomerCreate(CreateView):
    model = Customer
    template_name = 'customers/customers-create.html'
    fields = ['lead', 'contract']
    success_url = reverse_lazy('customers-list')


class CustomerDelete(DeleteView):
    model = Customer
    template_name = 'customers/customers-delete.html'
    success_url = reverse_lazy('customers-list')


class CustomerDetail(DetailView):
    model = Customer
    template_name = 'customers/customers-detail.html'


class CustomerUpdate(UpdateView):
    model = Customer
    template_name = 'customers/customers-edit.html'
    fields = ['lead', 'contract']
    success_url = reverse_lazy('customers-list')


class CustomerList(ListView):
    model = Customer
    template_name = 'customers/customers-list.html'
    context_object_name = 'customers'
