from django.urls import path
from .views import CustomerList, CustomerCreate, CustomerDelete, CustomerDetail, CustomerUpdate

urlpatterns = [
    path('', CustomerList.as_view(), name='customers-list'),
    path('new/', CustomerCreate.as_view(), name='customers-create'),
    path('<int:pk>/', CustomerDetail.as_view(), name='customers-detail'),
    path('<int:pk>/delete/', CustomerDelete.as_view(), name='customers-delete'),
    path('<int:pk>/edit/', CustomerUpdate.as_view(), name='customers-edit')
]