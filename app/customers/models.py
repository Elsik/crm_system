from django.db import models

from contracts.models import Contract
from leads.models import Lead


class Customer(models.Model):
    lead = models.ForeignKey(Lead, on_delete=models.CASCADE, verbose_name='Потенциальный клиент')
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE, verbose_name='Контракт')

    def __str__(self):
        return f'Потенциальный клиент№{self.id}: {self.lead.last_name} {self.lead.first_name}'
