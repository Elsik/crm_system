from django.db import models

from products.models import Product


class Advertisements(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    products = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Услуги')
    promotion = models.CharField(max_length=100, verbose_name='Канал продвижения')
    budget = models.CharField(max_length=100, default=0, verbose_name='Бюджет')

    def __str__(self):
        return f'Компания№{self.id}: {self.name}'
