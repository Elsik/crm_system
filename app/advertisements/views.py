from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, DeleteView, DetailView, UpdateView

from customers.models import Customer
from leads.models import Lead
from .models import Advertisements


class AdListView(ListView):
    template_name = 'ads/ads-list.html'
    model = Advertisements
    context_object_name = 'ads'


class AdCreateView(CreateView):
    model = Advertisements
    template_name = 'ads/ads-create.html'
    fields = ['name', 'products', 'promotion', 'budget']
    success_url = reverse_lazy('ads-list')


class AdDeleteView(DeleteView):
    model = Advertisements
    template_name = 'ads/ads-delete.html'
    success_url = reverse_lazy('ads-list')


class AdDetailView(DetailView):
    model = Advertisements
    template_name = 'ads/ads-detail.html'


class AdUpdateView(UpdateView):
    model = Advertisements
    template_name = 'ads/ads-edit.html'
    fields = ['name', 'products', 'promotion', 'budget']
    success_url = reverse_lazy('ads-list')


def adStatisticList(request):
    ads = Advertisements.objects.all()

    for ad in ads:
        leads_count = Lead.objects.filter(ads_company=ad).count()
        customers_count = Customer.objects.filter(lead__ads_company=ad).count()

        total_spent = int(ad.budget)
        contracts_amount = sum([customer.contract.cost for customer in Customer.objects.filter(lead__ads_company=ad)])

        ad.profit = total_spent - contracts_amount
        ad.save()

        ad.leads_count = leads_count
        ad.customers_count = customers_count

    return render(request, 'ads/ads-statistic.html', {'ads': ads})

