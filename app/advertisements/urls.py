from django.urls import path
from .views import AdListView, AdCreateView, AdDeleteView, AdDetailView, AdUpdateView, adStatisticList

urlpatterns = [
    path('', AdListView.as_view(), name='ads-list'),
    path('new/', AdCreateView.as_view(), name='add-create'),
    path('<int:pk>/delete/', AdDeleteView.as_view(), name='add-delete'),
    path('<int:pk>/', AdDetailView.as_view(), name='add-detail'),
    path('<int:pk>/edit/', AdUpdateView.as_view(), name='add-edit'),
    path('statistic/', adStatisticList, name='ads-statistic'),
]